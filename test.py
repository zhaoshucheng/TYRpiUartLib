import time
from TYRpiUartLib import UartLib


def main():
    lib_ = UartLib()
    lib_.UsbConfig(UartLib.USB_TYPE_2, UartLib.USB_PORT_2, True)
    time.sleep(0.5)
    lib_.Usb2ConfigAll(False)
    time.sleep(0.5)
    lib_.Usb3ConfigAll(True)
    time.sleep(0.5)
    lib_.CanMsgConfig(UartLib.CAN_PORT_2, 0x345, True, UartLib.CAN_CYCLE_200MS, 5, [0x01, 0xFF, 0x02, 0xFE, 0x03])
    time.sleep(0.5)
    lib_.CanMsgEcho(UartLib.CAN_PORT_1, 0x678)
    time.sleep(2)
    
    
    
if __name__ == '__main__':
    main()
    